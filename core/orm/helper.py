import os
import sys
import json
import time
from os import path

from sqlalchemy.orm import Session
from sqlalchemy.exc import InvalidRequestError
from fastapi.encoders import jsonable_encoder
from sqlalchemy.sql import func

from core.orm import Base, db, models, create_session, SessionLocal
from towerapp import schemas
from .models import DefenderTable, SessionTable, TowerTable
from tools.sender import Sender

import asyncio
import socketio


def create_all():
    import inspect
    import importlib
    _project_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
    #    model_modules = [m for m in os.listdir('{}/core/orm/models'.format(_project_path)) if
    #                     m != '__init__.py' and m[-3:] == '.py']

    #    model_modules = ('models',)

    #    for mm in model_modules:

    if True:
        import core.orm.models as model_module

        #        model_module = importlib.import_module('core.orm.models')
        for _name in dir(model_module):
            if not _name.startswith('__'):
                _class = getattr(model_module, _name)
                if inspect.isclass(_class):
                    globals().update({_name: _class})

    Base.metadata.create_all(db)


def alembic_create_all():
    from alembic.config import Config
    from alembic import command
    os.chdir('db')
    alembic_cfg = Config('alembic.ini')

    first_revision = command.show(alembic_cfg, "head")
    if first_revision is None:
        command.revision(alembic_cfg, autogenerate=True, message='initial')
    command.upgrade(alembic_cfg, "head")
    os.chdir('..')


mgr = socketio.AsyncAioPikaManager('amqp://rabbit')
sio = socketio.AsyncServer(client_manager=mgr)


# --------------------------------------- DEFENDER ----------------------------------------#

def db_add_defender(defender: schemas.OutputDefender):
    sess = create_session()
    is_created = create_session().query(DefenderTable).filter(DefenderTable.nickname == defender.nickname).first()
    if is_created:
        return is_created
    db_defender = models.DefenderTable(**defender.dict())
    sess.add(db_defender)
    try:
        sess.commit()
    except InvalidRequestError:
        sess.rollback()
        raise InvalidRequestError
    return {"Success"}


def db_get_defender_by_nickname(nickname: str):
    defender = create_session().query(DefenderTable).filter(DefenderTable.nickname == nickname).first().as_dict()
    if defender:
        return defender


def db_get_defenders():
    rows = create_session().query(DefenderTable.nickname).all()
    return [x[0] for x in rows]


def db_get_defender_statistics(nickname: str):
    defender = create_session().query(DefenderTable).filter(DefenderTable.nickname == nickname).first()
    if defender:
        return {'attack_points_generated': defender.attack_points_generated,
                'defense_points_generated': defender.defense_points_generated}
    return {'User is not find in DB!'}


def db_update_defender(defender: schemas.UpdateDefender):
    sess = create_session()
    defender = json.loads(defender)
    db_defender = sess.query(DefenderTable).filter(DefenderTable.nickname == defender["nickname"]).first()
    if defender["action"] == 'attack':
        setattr(db_defender, 'attack_points_generated', db_defender.attack_points_generated + 100)
    if defender["action"] == 'defend':
        setattr(db_defender, 'defense_points_generated', db_defender.defense_points_generated + 150)
    sess.commit()
    return db_defender


# ---------------------------------- TOWER ------------------------------------------------#


def db_add_tower(tower: dict):
    tower_table = TowerTable(**tower)
    SessionLocal.add(tower_table)
    try:
        SessionLocal.commit()
    except InvalidRequestError:
        SessionLocal.rollback()
        raise InvalidRequestError
    return tower_table


def db_get_tower_by_id(tower_id: int):
    tower = create_session().query(TowerTable).filter_by(id=tower_id).first()
    if tower:
        return tower.as_dict()


def db_tower_data_by_defense(defense: int):
    tower = create_session().query(TowerTable).filter_by(defense=defense).first()
    if tower:
        return tower
    return {'Tower not find in DB'}


def db_tower_get_shield(tower_id: int):
    shield = create_session().query(func.sum(DefenderTable.defense_points_generated).label("sum")).filter_by(
        tower_id=tower_id).first()
    return shield.sum


def db_update_tower(tower: schemas.UpdateTower):
    sess = create_session()
    db_tower = sess.query(TowerTable).filter(TowerTable.defense == tower["defense"]).first()
    if db_tower:
        if not ((tower["health"] == -500) & (db_tower.health < 500)):
            setattr(db_tower, 'health', db_tower.health + tower["health"])
            sess.commit()

        hocus_data = db_get_tower_by_id(1)
        pocus_data = db_get_tower_by_id(2)
        hocus_data["shield"] = db_tower_get_shield(1)
        pocus_data["shield"] = db_tower_get_shield(2)

        if hocus_data['health'] <= 0:
            asyncio.run(sio.emit("win", "YOU ARE WINNER!", to='Pocus'))
            asyncio.run(sio.emit("lost", "YOU ARE LOST", to='Hocus'))
            return 0
        if (pocus_data['health'] <= 0):
            asyncio.run(sio.emit("win", "YOU ARE WINNER!", to='Hocus'))
            asyncio.run(sio.emit("lost", "YOU ARE LOST", to='Pocus'))
            return 0

        asyncio.run(sio.emit('health', json.dumps({"health": hocus_data["health"]}), to='Hocus'))
        asyncio.run(sio.emit('health', json.dumps({"health": pocus_data["health"]}), to='Pocus'))

        asyncio.run(sio.emit('opp_health', json.dumps({"health": pocus_data["health"]}), to='Hocus'))
        asyncio.run(sio.emit('opp_health', json.dumps({"health": hocus_data["health"]}), to='Pocus'))

    return {'Tower not find in DB'}


if __name__ == '__main__':
    create_all()
    # alembic_create_all()
