import os
import pika
import json
import socketio
import asyncio


class Receiver:
    def __init__(self, exchange, exchange_type, binds, tower, sender):
        self.host = 'rabbit'
        self.port = 5672
        self.connection = self._create_connection()
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.binds = binds
        self.tower = tower
        self.sender = sender

    def __del__(self):
        self.connection.close()

    def _create_connection(self):
        parameters = pika.ConnectionParameters(host=self.host, port=self.port)
        return pika.BlockingConnection(parameters)

    def on_message_callback(self, channel, method, properties, body):
        if method.routing_key == 'update':
            data = json.loads(body.decode())
            hocus = data["hocus_tower"]
            pocus = data["pocus_tower"]
            if self.tower.tower_name == 'Hocus':
                self.tower.tower_health = hocus["health"]
                # self.tower.tower_shield = 0
                self.tower.opponent_health = pocus["health"]
            if self.tower.tower_name == 'Pocus':
                self.tower.tower_health = pocus["health"]
                # self.tower.tower_shield = 0
                self.tower.opponent_health = hocus["health"]

        if (method.routing_key == 'to_attack'):
            damage = -100
            if (self.tower.tower_shield > 0):
                self.tower.tower_shield += damage
                if (self.tower.tower_shield < 0):
                    damage = self.tower.tower_shield
                    self.tower.tower_shield = 0
                else: 
                    damage = 0

            mgr = socketio.AsyncAioPikaManager('amqp://rabbit')
            sio = socketio.AsyncServer(client_manager=mgr)
            asyncio.run( sio.emit("shield", json.dumps({"shield": self.tower.tower_shield}), to=self.tower.tower_name) )

            self.sender.publish('restlin', 'direct', 'update_tower',
                                json.dumps({"defense": self.tower.defense, "health": damage}))

    def setup(self):
        channel = self.connection.channel()
        channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type)
        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue
        for bind in self.binds:
            channel.queue_bind(
                exchange=self.exchange, queue=queue_name, routing_key=bind)

        print(
            ' [*] Waiting for data for ' + str(self.exchange) + ' To exit press CTRL+C   QUEUE NAME' + str(queue_name))
        channel.basic_consume(queue=queue_name, on_message_callback=self.on_message_callback)
        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
