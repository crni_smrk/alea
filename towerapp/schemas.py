import json

from typing import List, Optional
from pydantic import BaseModel

from fastapi import APIRouter
from fastapi import File, Form, UploadFile
from fastapi import Depends, HTTPException
from fastapi import Query

from sqlalchemy.orm import Session
import core.orm.models as models


router = APIRouter()


class OutputTower(BaseModel):
    id: int 
    health: int = 5000
    defense: int
    defender_count: int 
    
    session: int

    class Config:
        orm_mode = True


class OutputSession(BaseModel):
    id: int
    time_created: int 
    global_defender_count: int
    
    hocus_tower_id: int 
    pocus_tower_id: int 

    class Config:
        orm_mode = True

class OutputDefender(BaseModel):
    nickname: str 
    attack_points_generated: int  = 0
    defense_points_generated: int = 0
    
    tower_id: int = 1 

    class Config:
        orm_mode = True

class UpdateDefender(BaseModel):
    nickname: str 
    attack_points_generated: Optional[int]
    defense_points_generated: Optional[int]
    tower_id: Optional[int]

    class Config:
        orm_mode = True
    
class UpdateTower(BaseModel):
    health: Optional[int]
    defense: int
    session: Optional[int]
    defender_count: Optional[int]

    class Config:
        orm_mode = True
    



