import pika
from core.orm.helper import db_update_tower, db_update_defender
import json
import time


class Receiver:
    def __init__(self, exchange, exchange_type, binds):
        self.host = 'rabbit'
        self.port = 5672
        self.connection = self._create_connection()

        self.exchange = exchange
        self.exchange_type = exchange_type
        self.binds = binds

    def __del__(self):
        self.connection.close()

    def _create_connection(self):
        parameters = pika.ConnectionParameters(host=self.host, port=self.port)
        return pika.BlockingConnection(parameters)

    def on_message_callback(self, channel, method, properties, body):
        print('received new message for -' + str(channel) + ' message: ' + str(body.decode()))
        if method.routing_key == 'disconnect':
            data = json.loads(body.decode())
            if data["tower_name"] == 'Hocus':
                db_update_tower({'defense': 0, "health": -500})
            if data["tower_name"] == 'Pocus':
                db_update_tower({'defense': 1, "health": -500})

        if method.routing_key == 'update_tower':
            db_update_tower(json.loads(body.decode()))

        if method.routing_key == 'defender_attacks':
            db_update_defender(body.decode())

        if method.routing_key == 'defender_defend':
            db_update_defender(body.decode())

        if method.routing_key == 'connect':
            db_update_tower({'defense': 0, "health": 1000})
            db_update_tower({'defense': 1, "health": 1000})

    def setup(self):
        channel = self.connection.channel()
        channel.exchange_declare(exchange=self.exchange, exchange_type=self.exchange_type)

        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue

        for bind in self.binds:
            channel.queue_bind(
                exchange=self.exchange, queue=queue_name, routing_key=bind)
        print(
            ' [*] Waiting for data for ' + str(self.exchange) + ' To exit press CTRL+C   QUEUE NAME' + str(queue_name))

        channel.basic_consume(queue=queue_name, on_message_callback=self.on_message_callback)  # auto_ack=True
        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()
