import os
import csv
import argparse
import sys
import time

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from core.orm import create_session
from core.orm.models import DefenderTable, TowerTable

from sqlalchemy import exc
import alembic.config

def check_connection():
    sess = create_session()
    try:
        a = sess.execute("SELECT NOW()")
        return True
    except exc.OperationalError as e:
        return False

if __name__ == '__main__':
    # Wait for connection
    print("Trying to connect to db ...")
    while not check_connection():
        time.sleep(3)
        print("Retrying ...")
    print("Connection succeded !")

    # Check migrations
    #alembic.config.main(argv=[
    #    '--raiseerr',
    #    'upgrade', 
    #    'head'
    #])


    # Seed if necessary
    sess = create_session()

    res = sess.execute("SELECT count(*) FROM defender").fetchone()
    count_defender = res[0]

    res = sess.execute("SELECT count(*) FROM tower").fetchone()
    count_tower = res[0]

    if count_tower > 0:
        print(f"Not importing tower, {count_tower} records exist")
    else:
        defense = [0, 1] # 0 hocus, 1 pocus
        for i in defense:
            tower = TowerTable()
            tower.defense = i
            tower.health  = 5000
            tower.defender_count = 0
            sess.add(tower)
            sess.commit()
        print(f"Importing towers done")

    if count_defender > 0:
        print(f"Not importing defender, {count_defender} records exist")
    else:
        defender = DefenderTable()
        defender.nickname                   = 'hocus'
        defender.tower_id                   = 1
        defender.attack_points_generated    = 0
        defender.defense_points_generated   = 0 
        sess.add(defender)
        sess.commit()
        
        defender2 = DefenderTable()
        defender2.nickname                   = 'pocus'
        defender2.tower_id                   = 2
        defender2.attack_points_generated    = 0
        defender2.defense_points_generated   = 0 
        sess.add(defender2)
        sess.commit()

        print(f"Importing defenders done.")

