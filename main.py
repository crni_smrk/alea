import uvicorn
import json
from fastapi import FastAPI, Depends, Form, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials, APIKeyCookie
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder

import secrets
from jose import jwt

from core.orm.helper import *
from core.config import config
from core.orm import create_session, models
from core import logg
from core.orm import db

from starlette import status
from starlette.requests import Request
from starlette.responses import Response, HTMLResponse, RedirectResponse
from starlette.middleware.cors import CORSMiddleware

from towerapp import schemas

from tools.sender import Sender
from tools.receiver import Receiver
from threading import Thread

# Initialize Logging configuration from config yaml loader
logg.init()
log = logg.use('towerapp')

views = Jinja2Templates(directory="storage/views")

models.Base.metadata.create_all(bind=db)

# FastAPI application setup
app = FastAPI()

cookie_sec = APIKeyCookie(name="session")

secret_key = "testeste"

security = HTTPBasic()


def get_current_user(session: str = Depends(cookie_sec)):
    try:
        payload = jwt.decode(session, secret_key)
        user = users[payload["sub"]]
        return user
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Invalid authentication"
        )


app.add_middleware(CORSMiddleware,
                   allow_origins=['*'],
                   allow_methods=['*'],
                   allow_headers=['*'])


@app.middleware('http')
async def db_session_handler(request: Request, call_next):
    response = Response("Internal server error.", status_code=500)
    try:
        request.state.db = create_session()
        response = await call_next(request)
    except Exception as e:
        import traceback
        traceback.print_exc(50)
    finally:
        request.state.db.close()
    return response


@app.on_event('startup')
def startup_event():
    log.info("Starting app towerapp")


@app.get('/')
def get_index(request: Request):
    return views.TemplateResponse("our-very-cool-custom-vue-client.html", {"request": request})


@app.post("/login")
def login(response: Response, request: Request, nickname: str = Form(...)):
    users = db_get_defenders()
    print(users)
    print(nickname)
    if nickname not in users:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Invalid nickname"
        )
    token = jwt.encode({"sub": nickname}, secret_key)
    response.set_cookie("session", token)

    defender = db_get_defender_by_nickname(nickname)

    if defender["tower_id"] == 1:
        return views.TemplateResponse("hocus.html", {
            "request": request,
            "nickname": defender["nickname"]})

    return views.TemplateResponse("pocus.html", {
        "request": request,
        "nickname": defender["nickname"]})


# -----------------------------TOWER------------------------------------------
@app.post('/tower', summary='Get current tower data', tags=['Towers'])
# async def get_tower_data(defense: int, user: str = Depends(get_current_user)):
async def get_tower_data(defense: int):
    return db_tower_data_by_defense(defense)


@app.put('/towerupdate', summary='Update tower data', tags=['Towers'], response_model=schemas.UpdateTower)
# async def update_tower(tower: schemas.UpdateTower = Depends(get_current_user)):
async def update_tower(tower: schemas.UpdateTower):
    return db_update_tower(tower)


# ------------------------------DEFENDER -------------------------------------

# test get defenders
@app.get('/users')
def get_defenders():
    return db_get_defenders()


@app.post('/user', summary='Create Defender', tags=['Defenders'])
# async def create_user(defender: schemas.OutputDefender = Depends(get_current_user)):
async def create_user(defender: schemas.OutputDefender):
    return db_add_defender(defender)


@app.get('/userstatistics/{nickname}', summary='Get user`s statistics', tags=['Defenders'])
# async def get_user_statistics(nickname: str = Depends(get_current_user)):
async def get_user_statistics(nickname: str):
    return db_get_defender_statistics(nickname)


@app.put('/userupdate', summary='Update Defender', tags=['Defenders'], response_model=schemas.UpdateDefender)
# async def update_user(defender: schemas.UpdateDefender = Depends(get_current_user)):
async def update_user(defender: schemas.UpdateDefender):
    return db_update_user(defender)


def start_server():
    uvicorn.run("main:app",
                host='0.0.0.0',
                port=config.app.port,
                reload=config.app.hot_reload,
                debug=config.app.debug)


if __name__ == '__main__':
    sender = Sender()
    receiver = Receiver('restlin', 'direct',
                        ['defender_attacks', 'defender_defend', 'disconnect', 'connect', 'update_tower'])
    targets = [start_server, receiver.setup]

    for target in targets:
        t = Thread(target=target, args=())
        t.setDaemon(False)
        t.start()
